Assignment 1 
============

## Coursera 2014 - Introduction to Data Science

* twitter_fetch.py : To fetch tweets through Stream / Search API

* tweet_setniment.py : Analyse overall sentiment of tweets.

* term_sentiment.py : Predict sentiments of terms unlisted in AFINN-111.

* output.txt : sample output from Twitter's Stream API

* afinn_read.py : sample script to demonstrate how to read afinn scores.

* AFINN-111.txt : Pre-classified set of words, scored on basis of sentiments.

* frequency.py : Calculate Term-Frequency for each term in all tweets

* happiest_state.py : print the state code of happiest US state.

* top_ten.py : script to find top ten frequently used hashtags

